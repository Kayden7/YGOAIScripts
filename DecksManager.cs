﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DuelBot.Game.AI
{
    public static class DecksManager
    {
        public class DeckInstance
        {
            public string Deck { get; private set; }
            public Type Type { get; private set; }
            public string Level { get; private set; }

            public DeckInstance(string deck, Type type, string level)
            {
                Deck = deck;
                Type = type;
                Level = level;
            }
        }
        private static readonly Random _rand = new Random();
        public static readonly Dictionary<string, Type> DeckTypes = new Dictionary<string, Type>();

        public static void Init()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            Type[] types = asm.GetTypes();

            foreach (Type type in types)
            {
                MemberInfo info = type;
                object[] attributes = info.GetCustomAttributes(false);
                foreach (object attribute in attributes)
                {
                    if (attribute is DeckAttribute deck)
                    {
                        YGOServer.Deck.DeckCache.Add(type, YGOServer.Deck.Load(deck.Hash));
                        DeckTypes.Add(deck.Name, type);
                    }
                }
            }
            Logger.WriteLine("DuelBot Decks initialized, " + DeckTypes.Count + " found.");
        }

        public static Executor Instantiate(GameAI ai, Duel duel)
        {
            if (ai.Game.Deck < 0 || ai.Game.Deck >= DeckTypes.Count)
            {
                ai.Game.Deck = (short)_rand.Next(0, DeckTypes.Count);
            }

            Type t = DeckTypes.ElementAt(ai.Game.Deck).Value;
            Executor executor = Activator.CreateInstance(t, ai, duel) as Executor;
            executor.Deck = YGOServer.Deck.DeckCache[t];
            return executor;
        }
    }
}
