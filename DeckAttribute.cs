﻿using System;

namespace DuelBot.Game.AI
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DeckAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Hash { get; private set; }
        public string Level { get; private set; }

        public DeckAttribute(string name, string hash = null, string level = "Normal")
        {
            if (string.IsNullOrEmpty(hash))
            {
                hash = name;
            }

            Name = name;
            Hash = hash;
            Level = level;
        }
    }
}
