namespace DuelBot.Game.AI.Decks
{
    [Deck("Test_AI",
        "82Yw2rcJjl12ZTHCsIlTBhMMa86azwLDtjcuMcHwy/J6Bhhe2nyfCYaFklzgWFTOGI7PzZgIx494T7HCcIiVFQsMH90bxQrDze7ecBxjqQrHr3W5WWDYtuwEKwyXJwUzwLDf31ZWGJ5+qZEBhsUuicPxdSUVOL4k7cgIw82P7Zhg+IiLIisMAwA=",
        "TEST")]
    public class TestAiExecutor : DefaultExecutor
    {
        public class CardId
        {

        }

        public TestAiExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {

        }
    }
}
