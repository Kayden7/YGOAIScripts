namespace DuelBot.Game.AI.Decks
{
    [Deck("Mokey Mokey King",
        "M2QoTwqG43SmcCYYXmu1gQGGz8/5xArDDffz4DiQfwoLDEcekGKF4YgpWSwwfDpAFI4TkhcwwjDfCXXG5+lRjNF5qYxmS1WYvrTvYnJ8yMEcMSGe2bfyE4uIrCprmLgZa7DEbiYYdit0YYHhWeXscAwA",
        "Easy")]
    public class MokeyMokeyKingExecutor : DefaultExecutor
    {
        public class CardId
        {
            public const int LeoWizard = 4392470;
            public const int Bunilla = 69380702;
        }

        public MokeyMokeyKingExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.SpSummon, CardId.LeoWizard);
            AddExecutor(ExecutorType.SummonOrSet, CardId.Bunilla);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.Activate, DefaultField);
        }
    }
}