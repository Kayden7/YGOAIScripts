namespace DuelBot.Game.AI.Decks
{
    // NOT FINISHED YET
    [Deck("Graydle",
        "M+ffwWXECMPHrY/D8aXbE1lhePP3r8wwrDU1lRGGNxmdYYXh33dEGOfn/IDjxxu0mN+p/mN5bPaQFYa3yGgxwHDpjTLmLa9rWUCYOe8XEwwvTHvHAsO/WSqYdL4oM7PdO8dy7HgcE99aaWYQvv7YkqWhZjLLPuXlTI7LGVmvG61i5eq/zrrdy57V5tRURvnH91mvT17BktR+jAmG36sHMsCwg9omVhhenHWFCYbn6T5khWEA",
        "NotFinished")]
    public class GraydleExecutor : DefaultExecutor
    {
        public class CardId
        {
            public const int DarkHole = 53129443;
            public const int CosmicCyclone = 8267140;
            public const int SolemnJudgment = 41420027;
            public const int SolemnWarning = 84749824;
            public const int SolemnStrike = 40605147;
        }

        public GraydleExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.Activate, CardId.DarkHole, DefaultDarkHole);
            AddExecutor(ExecutorType.Activate, CardId.CosmicCyclone, DefaultCosmicCyclone);
            AddExecutor(ExecutorType.Activate, CardId.SolemnJudgment, DefaultSolemnJudgment);
            AddExecutor(ExecutorType.Activate, CardId.SolemnWarning, DefaultSolemnWarning);
            AddExecutor(ExecutorType.Activate, CardId.SolemnStrike, DefaultSolemnStrike);
            AddExecutor(ExecutorType.Activate, DefaultDontChainMyself);
            AddExecutor(ExecutorType.MonsterSet);
            AddExecutor(ExecutorType.SpSummon);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.SpellSet);
        }

        // will be added soon...?
    }
}