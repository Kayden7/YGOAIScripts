namespace DuelBot.Game.AI.Decks
{
    [Deck("Mokey Mokey",
        "02BIaI9mguGYin0MMLzGqp8JhjX7d7HA8ON8dgYYjrB+yQrDvRNMmGHYquINIwwnJC+A44s+F1lguHdKBcPPn9UMBQHLmYKeW7LErVKC49nuZgwwDAA=",
        "Easy")]
    public class MokeyMokeyExecutor : DefaultExecutor
    {
        public class CardId
        {
            public const int LeoWizard = 4392470;
            public const int Bunilla = 69380702;
        }

        public MokeyMokeyExecutor(GameAI ai, Duel duel)
            : base(ai, duel)
        {
            AddExecutor(ExecutorType.Summon);
            AddExecutor(ExecutorType.Repos, DefaultMonsterRepos);
            AddExecutor(ExecutorType.SpellSet);
        }
    }
}